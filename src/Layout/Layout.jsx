import React from "react";
import Header from "../Component/Header/Header";
import Footer from "../Page/HomePage/Footer/Footer";

export default function Layout({ Component }) {
  return (
    <div className="space-y-20">
      <Header />
      <Component />
      <Footer />
    </div>
  );
}
