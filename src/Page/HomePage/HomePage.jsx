import React from "react";
import ListMovie from "./ListMovie/ListMovie";

import TabMovie from "./TabMovie/TabMovie";
import CarouselMovie from "./CarouselMovie/CarouselMovie";

export default function HomePage() {
  return (
    <div>
      <h1 className="font-bold ">
        <div>
          <CarouselMovie />
        </div>
        <div>
          <ListMovie />
        </div>
        <div>
          <TabMovie />
        </div>
      </h1>
    </div>
  );
}
