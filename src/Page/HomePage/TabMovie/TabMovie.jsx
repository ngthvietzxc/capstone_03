import React, { useEffect } from "react";
import { Tabs } from "antd";

import ChildrenTabMovie from "./ChildrenTabMovie";
import { Container } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getTheater } from "../../../redux/action/movieAction";

export default function TabMovie() {
  const dispatch = useDispatch();
  const movies = useSelector((state) => state.movieReducer.movies);

  useEffect(() => {
    dispatch(getTheater());
  }, [dispatch]);
  let renderHeThongRap = () => {
    return movies.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="" />,

        children: (
          <Tabs
            style={{ height: 600 }}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.diaChi,
                label: (
                  <p
                    className="flex items-center justify-center"
                    style={{ height: "64px" }}
                  >
                    {cumRap.tenCumRap}
                  </p>
                ),
                children: <ChildrenTabMovie listMovie={cumRap.danhSachPhim} />,
              };
            })}
          />
        ),
      };
    });
  };
  //   moment js npm
  return (
    <div>
      <Container className="MuiContainer-root pt-5 ">
        <h2 className="text-3xl bg-red-600 py-2 px-4 mb-10 rounded text-white m-5">
          LỊCH CHIẾU PHIM
        </h2>
        <Tabs
          style={{ height: 650 }}
          className="pt-5 border-black rounded-lg border-solid border-2 m-5"
          tabPosition="left"
          defaultActiveKey="1"
          items={renderHeThongRap()}
        />
      </Container>
    </div>
  );
}
