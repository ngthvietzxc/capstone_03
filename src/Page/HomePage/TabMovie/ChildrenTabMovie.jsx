import moment from "moment";
import React from "react";

export default function ChildrenTabMovie({ listMovie }) {
  return (
    <div style={{ height: 600 }} className="overflow-y-auto">
      {listMovie.map((phim) => {
        return (
          <div className="border-black rounded-lg border-solid border-2 m-3">
            <h5 className="font-medium text-xl pt-5">{phim.tenPhim}</h5>
            <div className="flex  p-5 m-5">
              <img className="w-36 h-40 pr-5" src={phim.hinhAnh} alt="" />
              <div>
                <div className="grid gap-5 grid-cols-2">
                  {phim.lstLichChieuTheoPhim.slice(0, 6).map((lich, index) => {
                    return (
                      <span
                        key={index}
                        className="btn px-3 py-2 font-semibold border rounded dark:border-gray-100 dark:text-green-600 "
                      >
                        {moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}{" "}
                        <span className="text-red-500 !important">
                          {moment(lich.ngayChieuGioChieu).format("HH:MM A")}
                        </span>
                      </span>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
