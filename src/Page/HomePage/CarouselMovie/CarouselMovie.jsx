import React, { useEffect } from "react";

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useDispatch, useSelector } from "react-redux";
import { getBanner } from "../../../redux/action/movieAction";

const settings = {
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 1500,
};

export default function CarouselMovie() {
  const dispatch = useDispatch();
  const banner = useSelector((state) => state.movieReducer.banners);

  useEffect(() => {
    dispatch(getBanner());
  }, [dispatch]);

  return (
    <div className="pb-5">
      <Slider {...settings}>
        {banner.map((banner, index) => (
          <div key={index}>
            <img
              alt=""
              src={banner.hinhAnh}
              style={{ width: "100%", height: "600px", objectFit: "cover" }}
              className="object-top"
            />
          </div>
        ))}
      </Slider>
    </div>
  );
}
