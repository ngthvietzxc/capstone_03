import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function CardMovie({ movieCard }) {
  let { tenPhim, hinhAnh } = movieCard;
  return (
    <div style={{ margin: 20 }}>
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img
            alt="example"
            src={hinhAnh}
            style={{
              objectFit: "cover",
              height: "300px",
              objectPosition: "top",
            }}
          />
        }
      >
        <Meta className="pb-6" title={tenPhim} />
        <NavLink
          className="bg-black tracking-wider hover:bg-red-600 hover:text-white text-white font-bold py-2 px-10 rounded w-full "
          to={`/detail/${movieCard.maPhim}`}
        >
          Đặt vé
        </NavLink>
      </Card>
    </div>
  );
}
