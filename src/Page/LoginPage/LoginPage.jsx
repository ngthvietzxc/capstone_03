import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input, message } from "antd";
import "./LoginPage.css";

import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

import { localUserService } from "../../services/localServices";
import { setUserActionThunk } from "../../redux/action/userAction";
const App = () => {
  // useNavigate() là một hook được cung cấp bởi thư viện React Router, nó được sử dụng để điều hướng trong các ứng dụng web.
  let navigate = useNavigate();
  let dispatch = useDispatch();

  const onFinishThunk = (values) => {
    let handleSuccess = (res) => {
      message.success("Đăng nhập thành công");
      localUserService.set(res.data.content);
      navigate(`/`);
    };
    dispatch(setUserActionThunk(values, handleSuccess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("🚀 ~ App ~ errorInfo:", errorInfo);
  };

  // abc123 123456
  return (
    <div className="login-page">
      <div className="login-container w-1/2">
        <h1 className="font-bold text-2xl pb-5">Đăng nhập</h1>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinishThunk}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            // api lấy name: taiKhoan
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng điền tên đăng nhập",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Tên đăng nhập"
            />
          </Form.Item>
          <Form.Item
            // api lấy name: matKhau
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Vui lòng điền mật khẩu",
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Mật khẩu"
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox style={{ float: "left" }}>Nhớ tên đăng nhập</Checkbox>
            </Form.Item>
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Đăng nhập
            </Button>
          </Form.Item>
          <div>
            <NavLink to="/register">
              <a style={{ float: "right" }} href="aa">
                Bạn chưa có tài khoản? Đăng ký
              </a>
            </NavLink>
          </div>
        </Form>
      </div>
    </div>
  );
};
export default App;
