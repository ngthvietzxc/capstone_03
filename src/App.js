import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import HomePage from "./Page/HomePage/HomePage";
import LoginPage from "./Page/LoginPage/LoginPage";
import DetailPage from "./Page/DetailPage/DetailPage";
import Layout from "./Layout/Layout";
import RegisterPage from "./Page/RegisterPage/RegisterPage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<Layout Component={LoginPage} />} />
          <Route
            path="/register"
            element={<Layout Component={RegisterPage} />}
          />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
