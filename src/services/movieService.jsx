import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const movieService = {
  getMovieList: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
      method: "GET",
      headers: configHeaders(),
    });
    // Cách viết khác: return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim`,headers: congifHeaders(),});
  },
  getMovieByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP06`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  getDetailMovie: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  getBannerList: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachBanner?maNhom=GP07`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  getLichChieu: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
};
