import { userService } from "../../services/userService";
import { USER_LOGIN } from "./../constant/userConstant";

export const setUserAction = (value) => {
  return {
    type: USER_LOGIN,
    payload: value,
  };
};
// redux-thunk: gọi api trực tiếp trong action

export const setUserActionThunk = (formData, onSuccess) => {
  return (dispatch) => {
    userService
      .postLogin(formData)
      .then((res) => {
        console.log(res);
        dispatch({ type: USER_LOGIN, payload: res.data.content });
        onSuccess(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
