import { hardcodedBanners } from "../../Page/HomePage/CarouselMovie/dataBanner";
import { movieService } from "./../../services/movieService";
import {
  GET_BANNER,
  GET_LICH_CHIEU,
  GET_THEATER,
} from "./../constant/movieConstant";

export const getTheater = () => (dispatch) => {
  movieService
    .getMovieByTheater()
    .then((response) => {
      dispatch({
        type: GET_THEATER,
        payload: response.data.content,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getBanner = () => (dispatch) => {
  movieService
    .getBannerList()
    .then((response) => {
      const banners = response.data.content.concat(hardcodedBanners);
      dispatch({
        type: GET_BANNER,
        payload: banners,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};
export const getLichChieuAction = (id) => (dispatch) => {
  movieService
    .getLichChieu(id)
    .then((response) => {
      dispatch({
        type: GET_LICH_CHIEU,
        payload: response.data.content,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};
